//Ejercicio 1
$(document).on("click", ".circulo", function() {
  $(this).toggleClass("naranja");
});

$("#ponRojoBtn").click(function() {
  $(".circulo").addClass("rojo");
});

$("#quitaRojoBtn").click(function() {
  $(".circulo").removeClass("rojo");
});

//Ejercicio 2
$(document).on("click", "#copiaBtn", function() {
  let valor = $("#origen").val();

  $("#destino").val(valor);
});

//Ejercicio 3
$(document).on("click", "#reemplazaBtn", function() {
  let valor = $("#origen2").val();

  $(".celda").text(valor);
});

$(document).on("click", "#addBtn", function() {
  let valor = $("#origen2").val();

  $("#origen2").val("");
  $(".celda").append(valor);
});

//Ejercicio 4
$(document).on("click", "#menosBtn", function() {
  let valor = $("#contador").val();

  if (!(valor <= 0)) {
    $("#contador").val(--valor);
  }
});

$(document).on("click", "#masBtn", function() {
  let valor = $("#contador").val();

  if (!(valor >= 10)) {
    $("#contador").val(++valor);
  }
});

//Ejercicio 5
//Funcion que devuelve si una array de elementos contiene un valor
function contiene(valor, arr) {
  for (let i = 0; i < arr.length; i++) {
    if ($(arr[i]).text() == valor) {
      return true;
    }
  }
  return false;
}

//Boton next
$(document).on("click", "#next", function() {
  let casillas = $("li.page-item.pagina a");

  if (!contiene("20", casillas)) {
    for (let i = 0; i < casillas.length; i++) {
      let valorActual = parseInt($(casillas[i]).text());
      $(casillas[i]).text(valorActual + 1);
      $(casillas[i]).attr("href", `#p${valorActual + 1}`);
    }
  }
});

//Boton anterior
$(document).on("click", "#previous", function() {
  let casillas = $("li.page-item.pagina a");

  if (!contiene("1", casillas)) {
    for (let i = 0; i < casillas.length; i++) {
      let valorActual = parseInt($(casillas[i]).text());
      $(casillas[i]).text(valorActual - 1);
      $(casillas[i]).attr("href", `#p${valorActual - 1}`);
    }
  }
});

//Click en numeros de pagina
$(document).on("click", "li.page-item.pagina", function() {
  let pagina = $(this)
    .children("a")
    .text();

  alert(`Redirigiendo a pagina ${pagina}`);
});
