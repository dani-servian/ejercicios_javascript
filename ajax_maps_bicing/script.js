$(function() {
  window.onload = function() {
    let markers = []; //guarda markers para poder administrarlos despues
    let infoWindows = [];

    //Cierra todas las ventanas de informacion
    function closeWindows() {
      for (let i = 0; i < infoWindows.length; i++) {
        const infoWindow = infoWindows[i];
        infoWindow.close();
      }
    }

    //limpia todos los markers
    function limpiarMarkers() {
      for (let i = 0; i < markers.length; i++) {
        const marker = markers[i];
        marker.setMap(null);
      }
    }

    //Listener para el click en el boton de carga
    $(document).on("click", "#loadBtn", function() {
      limpiarMarkers();
      let input = $("#inputNumeroDeBicis").val(); //Obtiene el valor que ha introducido el usuario

      //si el usuario no ha introducido un numero o introduce un numero menor a 0 se le alerta
      if (input > 0) {
        //Llama por ajax a la api de bicing para obtener los datos
        $.getJSON("https://api.citybik.es/v2/networks/bicing", function(datos) {
          let stations = datos.network.stations; //crea una array con todas las estaciones bicing
          /**
           * Itera cada estacion. Obtiene el nombre, las bicis disponibles,
           * los slots libres, latitud y longitud.
           * Al obtener los datos los añade a la tabla con la id #tBody
           * */
          for (let i = 0; i < stations.length; i++) {
            const station = stations[i]; //almacena cada estacion individualmente

            if (station.free_bikes >= input) {
              let name = station.name; //nombre de la estacion
              let bikes = station.free_bikes; //bicicletas disponibles  123 - c/ Bruc
              let slots = station.empty_slots; //slots libres
              let latitude = station.latitude; //latitud
              let longitude = station.longitude; //longitud

              //crea un nuevo marker con la informacion obtenida
              let marker = new google.maps.Marker({
                position: new google.maps.LatLng(latitude, longitude),
                map: map,
                title: name
              });

              //Contenido que se asociará a la etiqueta del marcador actual (en forma de string)
              let contentString = `<h5 class="display-5">${name}</h5>
                                   <p class="lead d-inline">Numero de bicis: ${bikes}</p>
                                   <p class="lead d-inline ml-3">Slots disponibles: ${slots}</p>`;

              //Crea el objeto etiqueta
              let infowindow = new google.maps.InfoWindow({
                content: contentString
              });

              //Asocia el click en el marcador con la etiqueta creada anteriormente
              marker.addListener("click", function() {
                closeWindows(); //Cierra el resto de ventanas
                infowindow.open(map, marker); //Al hacer click se abre
              });

              infoWindows.push(infowindow); //guarda cada infowindow
              markers.push(marker); //guarda cada marker
            }
          }
        });
      } else {
        //en el caso de que no se introduzca un numero se alerta al usuario
        alert("Por favor introduce un numero superior a 0.");
      }
    });
  };
});
