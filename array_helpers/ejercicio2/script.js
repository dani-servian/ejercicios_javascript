let stations = [];
let markers = [];

//Función que nos devuelva la estación con más bicicletas libres.
function estacionConMasHuecos() {
  let masAMenosLibres = (a, b) => {
    if (a.empty_slots < b.empty_slots) return 1;
    if (a.empty_slots > b.empty_slots) return -1;
    return 0;
  };
  stations.sort(masAMenosLibres); //ordena las estaciones de mas a menos huecos libres.
  return stations[0]; //Devuelve la estacion con mas huecos libres
}

//Función que nos devuelva un array de nombres de estaciones que no tienen bicicletas libres
function sinBicisLibres() {
  let sinBicis = []; //Array que almacena los nombres de las estaciones sin bicis
  stations
    .filter(station => station.free_bikes === 0) //Filtra las estaciones que tengan free_bikes = 0
    .forEach(station => {
      sinBicis.push(station.name); //Añade cada estacion con 0 bikes a la array sinBicis
    });

  return sinBicis; //Devuelve la array sinBicis
}

//Función que devuelva las X estaciones más cercanas a la ubicación 41.388163, 2.179769.
//Devuelve la distancia entre dos puntos
function distancia(latA, lonA, latB, lonB) {
  return Math.sqrt(Math.pow(latA - latB, 2) + Math.pow(lonA - lonB, 2));
}

//Compara dos distancias (funcion auxiliar sort)
function ordenDistancia(a, b) {
  if (a.distancia > b.distancia) return 1;
  if (a.distancia < b.distancia) return -1;
  return 0;
}

//Devuelve las X estaciones más cercanas a una latitud y longitud
function estacionesMasCercanas(numEstaciones, lat = 41.388163, lon = 2.179769) {
  let result = []; //Array que almacena el resultado

  //Crea una array que añade la distancia entre las coordenadas introducidas y la posicion de cada estacion.
  let estacionesConDistancia = stations.map(s => {
    s.distancia = distancia(lat, lon, s.latitude, s.longitude);
    return s;
  });

  estacionesConDistancia.sort(ordenDistancia); //Ordena las estaciones de menor a mayor distancia

  //Añade al resultado el numero introducido de estaciones
  for (let i = 0; i < numEstaciones; i++) {
    result.push(estacionesConDistancia[i]);
  }

  return result; //Devuelve el resultado que contiene las estaciones mas cercanas.
}

$(function() {
  window.onload = function() {
    let markers = []; //guarda markers para poder administrarlos despues
    let infoWindows = [];

    //Cierra todas las ventanas de informacion
    function closeWindows() {
      for (let i = 0; i < infoWindows.length; i++) {
        const infoWindow = infoWindows[i];
        infoWindow.close();
      }
    }

    //limpia todos los markers
    function limpiarMarkers() {
      for (let i = 0; i < markers.length; i++) {
        const marker = markers[i];
        marker.setMap(null);
      }
    }

    //Listener para el click en el boton de carga
    $(document).on("click", "#loadBtn", function() {
      limpiarMarkers();
      let lat = $("#inputLat").val(); //Obtiene el valor que ha introducido el usuario
      let lon = $("#inputLon").val(); //Obtiene el valor que ha introducido el usuario
      let numEstaciones = $("#inputNumEstaciones").val(); //Obtiene el valor que ha introducido el usuario

      $.getJSON("https://api.citybik.es/v2/networks/bicing", function(datos) {
        stations = datos.network.stations; //crea una array con todas las estaciones bicing
        let estacionesCercanas = estacionesMasCercanas(numEstaciones, lat, lon);

        estacionesCercanas.forEach(station => {
          let name = station.name; //nombre de la estacion
          let bikes = station.free_bikes; //bicicletas disponibles  123 - c/ Bruc
          let slots = station.empty_slots; //slots libres
          let latitude = station.latitude; //latitud
          let longitude = station.longitude; //longitud

          //crea un nuevo marker con la informacion obtenida
          let marker = new google.maps.Marker({
            position: new google.maps.LatLng(latitude, longitude),
            map: map,
            title: name
          });

          //Contenido que se asociará a la etiqueta del marcador actual (en forma de string)
          let contentString = `<h5 class="display-5">${name}</h5>
          <p class="lead d-inline">Numero de bicis: ${bikes}</p>
          <p class="lead d-inline ml-3">Slots disponibles: ${slots}</p>`;

          //Crea el objeto etiqueta
          let infowindow = new google.maps.InfoWindow({
            content: contentString
          });

          //Asocia el click en el marcador con la etiqueta creada anteriormente
          marker.addListener("click", function() {
            closeWindows(); //Cierra el resto de ventanas
            infowindow.open(map, marker); //Al hacer click se abre
          });

          infoWindows.push(infowindow); //guarda cada infowindow
          markers.push(marker); //guarda cada marker
        });
      });
    });
  };
});
