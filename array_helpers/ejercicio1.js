//Carga la array de motos
let MOTOS = require("./motos");

//La moto mas barata y la mas cara
//Ordena la array MOTOS por precio
MOTOS.sort((a, b) => {
  if (a.preu < b.preu) return -1;
  if (a.preu > b.preu) return 1;
  return 0;
});

//Como hemos ordenado la array por precio devuelve el primer elemento de esta
console.log(
  `La moto mas barata es la ${MOTOS[0].model} que cuesta ${
    MOTOS[0].preu
  } euros.`
);

//Devuelve el ultimo elemento de la array ordenada por precio
console.log(
  `La moto mas cara es la ${MOTOS[MOTOS.length - 1].model} que cuesta ${
    MOTOS[MOTOS.length - 1].preu
  } euros.`
);

//¿Cuantas motos hay con menos de 30000km de la marca HONDA?
//Crea una nueva array con las motos que coinciden con el filtro
let hondasMenos30k = MOTOS.filter(
  moto => moto.model.indexOf("HONDA") > -1 && moto.kilometres < 30000
);

//Muestra el numero de motos por consola
console.log(`Hay ${hondasMenos30k.length} motos HONDA con menos de 30000km.`);

//¿Cuantas motos hay con menos de 30000km y mas de 240cc?
//Crea una nueva array con las motos que coinciden con el filtro
let mas240ccMenos30k = MOTOS.filter(
  moto => moto.cilindrada > 240 && moto.kilometres < 30000
);

//Muestra el numero de motos por consola
console.log(
  `Hay ${mas240ccMenos30k.length} motos con mas de 240cc y menos de 30000km.`
);

/**
 * ¿Qué moto tiene menos de 25.000km, más de 350cc de cilindrada
 * y cuesta entre 1.800 y 2.200 eur ?
 *  */
//Itera la array MOTOS
MOTOS.forEach(moto => {
  if (
    moto.kilometres < 25000 &&
    moto.cilindrada > 350 &&
    (moto.preu < 2200) & (moto.preu > 1800)
  ) {
    //muestra la moto que cumple las condiciones
    console.log(
      `La Moto con menos de 25.000km, más de 350cc de cilindrada y que cuesta entre 1.800 y 2.200 eur es la ${
        moto.model
      }.`
    );
  }
});

//Lista de marcas distintas con el numero de motos
let marcas = []; //Contiene objetos marca y cantidad

/**
 * Para cada moto prueba si la marca ya esta registrada,
 * en caso de que esté le añade 1 a la cantidad de motos.
 * En caso de que la marca no exista la registra con cantidad 1.
 */
MOTOS.forEach(moto => {
  let marca = moto.model.split(" ")[0]; //Obtiene la marca de la moto
  let indiceMarca = marcas.findIndex(el => el.marca === marca); //Obtiene el indice (en caso de existir) de la marca en la array de marcas
  if (indiceMarca === -1) {
    marcas.push({ marca: marca, cantidad: 1 }); //Si no existe añade la marca con cantidad 1
  } else {
    marcas[indiceMarca].cantidad++; //Si ya existe incrementa en 1 la cantidad de motos
  }
});

//Muestra la array marcas por consola
marcas.forEach(el => console.log(`${el.marca}: ${el.cantidad} motos.`));
