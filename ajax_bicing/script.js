//Listener para el click en el boton de carga
$(document).on("click", "#loadBtn", function() {
  let input = $("#inputNumeroDeBicis").val(); //Obtiene el valor que ha introducido el usuario

  //si el usuario no ha introducido un numero o introduce un numero menor a 0 se le alerta
  if (input > 0) {
    //Llama por ajax a la api de bicing para obtener los datos
    $.getJSON("https://api.citybik.es/v2/networks/bicing", function(datos) {
      let stations = datos.network.stations; //crea una array con todas las estaciones bicing
      $("#tBody").html(""); //limpia los resultados anteriores de la tabla
      /**
       * Itera cada estacion. Obtiene el nombre, las bicis disponibles,
       * los slots libres, latitud y longitud.
       * Al obtener los datos los añade a la tabla con la id #tBody
       * */
      for (let i = 0; i < stations.length; i++) {
        const station = stations[i]; //almacena cada estacion individualmente

        if (station.free_bikes >= input) {
          let name = $("<td>").text(station.name); //nombre de la estacion
          let bikes = $("<td>").text(station.free_bikes); //bicicletas disponibles
          let slots = $("<td>").text(station.empty_slots); //slots libres
          let latitude = $("<td>").text(station.latitude); //latitud
          let longitude = $("<td>").text(station.longitude); //longitud

          $("#tBody").append(
            $("<tr>").append(name, bikes, slots, latitude, longitude) //añade un tr con todos los datos obtenidos.
          );
        }
      }
    });
  } else {
    alert("Por favor introduce un numero superior a 0.");
  }
});
