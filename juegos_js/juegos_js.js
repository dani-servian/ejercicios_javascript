function trio() {
  let valor1 = Math.floor(Math.random() * 50);
  let valor2 = Math.floor(Math.random() * 50);
  let valor3 = Math.floor(Math.random() * 50);

  $("#circulo1")
    .children(".numero")
    .text(valor1);
  $("#circulo2")
    .children(".numero")
    .text(valor2);
  $("#circulo3")
    .children(".numero")
    .text(valor3);
}

function piedraPapelTijera(eleccion) {
  const posibles = [
    { valor: "Piedra", icono: "fa-hand-rock" },
    { valor: "Papel", icono: "fa-hand-paper" },
    { valor: "Tijera", icono: "fa-hand-peace" }
  ];
  let resultado = "Has perdido";
  let jugada = posibles[Math.floor(Math.random() * 3)];
  switch (jugada.valor) {
    case "Piedra":
      if (eleccion === "Papel") {
        resultado = "¡Has ganado!";
      } else if (eleccion == "Piedra") {
        resultado = "Has empatado";
      }
      break;
    case "Papel":
      if (eleccion === "Tijera") {
        resultado = "¡Has ganado!";
      } else if (eleccion == "Papel") {
        resultado = "Has empatado";
      }
      break;
    case "Tijera":
      if (eleccion === "Piedra") {
        resultado = "¡Has ganado!";
      } else if (eleccion == "Tijera") {
        resultado = "Has empatado";
      }
      break;
  }

  $("#resultado").text(resultado);
  $("#resultado2").html(
    `<div>El ordenador ha elegido:</div><div><i class="fas ${
      jugada.icono
    } fa-4x"></i></div>`
  );
}

function cuentaAtras(numero, eleccion) {
  if (numero > 0) {
    $("#resultado2").text("");
    $("#resultado").text(numero);
    setTimeout(function() {
      cuentaAtras(numero - 1, eleccion);
    }, 1000);
  } else {
    piedraPapelTijera(eleccion);
  }
}
