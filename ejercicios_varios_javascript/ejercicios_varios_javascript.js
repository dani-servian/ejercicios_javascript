//Devuelve el mayor de los dos numeros introducidos:
function mayor(numeroA, numeroB) {
  if (numeroA > numeroB) {
    //Si a es mayor que b devuelve a
    return numeroA;
  } else {
    //Si b es mayor que a devuelve b
    return numeroB;
  }
}

//Devuelve datos sobre el numero introducido:
function datos(numero) {
  let esPar, esDivisiblePor3, esDivisiblePor5, esDivisiblePor7;

  numero % 2 === 0 ? (esPar = true) : (esPar = false); //Comprueba si el numero es par
  numero % 3 === 0 ? (esDivisiblePor3 = true) : (esDivisiblePor3 = false); //Comprueba si el numero es divisible por 3
  numero % 5 === 0 ? (esDivisiblePor5 = true) : (esDivisiblePor5 = false); //Comprueba si el numero es divisible por 5
  numero % 7 === 0 ? (esDivisiblePor7 = true) : (esDivisiblePor7 = false); //Comprueba si el numero es divisible por 7

  //Concatena los datos y los devuelve
  return (
    `${numero} es ${esPar ? "par" : "impar"}\n` +
    `${esDivisiblePor3 ? "SI" : "NO"} es divisible por 3\n` + //Segun si es divisible devuelve "SI" o "NO"
    `${esDivisiblePor5 ? "SI" : "NO"} es divisible por 5\n` +
    `${esDivisiblePor7 ? "SI" : "NO"} es divisible por 7`
  );
}

//Devuelve la suma de la array de numeros introducida:
function sumaValores(arr) {
  let total = 0; //Se utiliza para almacenar el total

  arr.forEach(numero => {
    //Itera toda la array sumando cada numero al total
    total += numero;
  });

  return total; //Devuelve la suma de todos los numeros
}

//Devuelve el factorial del numero introducido:
function factorial(numero) {
  let resultado = numero; //Para almacenar el resultado

  /**
   * El factorial de un numero es el producto de todos los enteros positivos inferiores a este.
   * Para ello utilizamos el bucle for contando del numero a 2.
   * Ej: para el numero 5 ==> "5 * 4 * 3 * 2 = 600"
   */
  for (let i = numero; i > 1; i--) {
    resultado *= i; //Multiplicamos el numero introducido por los enteros inferiores a este
  }

  return resultado; //devuelve el resultado
}

//Devuelve la palabra introducida con la primera letra en mayuscula
function capitaliza(palabra) {
  return palabra.charAt(0).toUpperCase() + palabra.slice(1).toLowerCase(); //Obtiene la primera letra, la hace mayuscula y le añade el resto de la palabra
}

//Devuelve informacion sobre la palabra introducida
function palabra(palabra) {
  let esPar,
    vocales = 0,
    consonantes = 0; //Almacenan si el numero es par, el largo de la palabra, vocales y consonantes

  let largo = palabra.length; //Obtiene el largo de la palabra

  //Pasa por cada letra aplicando una expresion regex que comprueva si es una vocal
  for (let i = 0; i < palabra.length; i++) {
    if (/^[aeiou]$/i.test(palabra[i])) {
      vocales++; //Si es vocal incrementa el contador de las vocales
    } else {
      consonantes++; //Si es consonante incrementa el contador de las consonantes
    }
  }

  esPar = largo % 2 === 0 ? true : false; //Comprueva si el largo de la palabra es un numero par

  //Devuelve los resultados concatenados en una string
  return (
    `"${palabra}" tiene ${largo} letras\n` +
    `${largo} es un numero ${esPar ? "par" : "impar"}\n` +
    `Vocales: ${vocales}\n` +
    `Consonantes: ${consonantes}`
  );
}

//Escribe en consola el dia de la semana
function hoy() {
  let date = new Date(); //Crea un objeto Date
  //Define una array con los dias de la semana
  let dias = [
    "domingo",
    "lunes",
    "martes",
    "miercoles",
    "jueves",
    "viernes",
    "sabado"
  ];
  console.log(`Hoy es ${dias[date.getDay()]}`); //Escribe en consola
}

//Escribe en consola los dias que faltan para navidad
function navidad() {
  let date = new Date(); //Crea un objeto Date
  let navidad = new Date(date.getFullYear(), 11, 25); //Crea un objeto date con la fecha de navidad
  let hoy = new Date(date.getFullYear(), date.getMonth(), date.getDate()); //Crea un objeto Date con la fecha de hoy

  let msNavidad = navidad.getTime(); //Obtiene el tiempo en milisegundos de Navidad
  let msHoy = hoy.getTime(); //obtiene el tiempo en milisegundos de hoy

  //Al tiempo de navidad le resta el tiempo de hoy y convierte los milisegundos a dias
  let diasQueFaltan = parseInt((msNavidad - msHoy) / 1000 / 60 / 60 / 24);

  console.log(`Faltan ${diasQueFaltan} dias para Navidad.`);
}

//Muestra por consola la suma de los numeros, el maximo y el minimo de una array
function analiza(arr) {
  let total = 0; //Almacena el total de los numeros
  let mayor, menor; //Almacenan el numero mayor y menor de la array
  let largo = arr.length; //Obtiene el largo de la array

  //Itera por todos los elementos de la array
  for (let i = 0; i < arr.length; i++) {
    const numero = arr[i];
    //Si no es la primera vez que se ejecuta el loop hacemos las comprovaciones de mayor y menor, si es la primera asignamos mayor y menor.
    if (i !== 0) {
      if (numero > mayor) {
        //Si el numero es mas grande que el mayor se almacena en este
        mayor = numero;
      }
      if (numero < menor) {
        //Si el numero es inferior al menor se almacena en este
        menor = numero;
      }
    } else {
      //Si es la primera vez que se ejecuta el for asignamos el numero a mayor y menor
      mayor = numero;
      menor = numero;
    }
    total += numero; //Siempre sumamos el numero a la array
  }

  //Muestra los resultados obtenidos por consola
  return (
    `La suma de los ${largo} numeros es ${total}.\n` +
    `El numero mayor es ${mayor}\n` +
    `El numero menor es ${menor}\n`
  );
}

//Devuelve si el numero introducido es primo
function primo(numero) {
  let esPrimo = true; //Se asume que el numero va a ser primo de entrada, si resulta no serlo se cambiara

  //Itera todos los numeros del numero anterior al introducido hasta 2
  for (let i = numero - 1; i > 1; i--) {
    if (numero % i === 0) {
      //Si en algun caso el numero es divisible significa que no es primo.
      esPrimo = false;
      break; //Sale del bucle for
    }
  }

  //Devuelve el resultado
  //return `${numero} ${esPrimo ? "" : "no "}es primo`; *Modificado para el ultimo ejercicio
  return esPrimo;
}

//Devuelve el numero de digitos introducido de la serie fibonacci
function fibonacci(digitos) {
  /**
   * En la serie Fibonacci cada digito es la suma de los dos anteriores.
   * Ej: "1,1,2,3,5"
   */
  let serieFibonacci = [0, 1]; //Array que contiene los resultados, se inicializa con los dos primeros digitos

  //Suma los dos digitos anteriores y añade el resultado a la array
  for (let i = 0; i < digitos - 1; i++) {
    serieFibonacci.push(
      serieFibonacci[serieFibonacci.length - 2] + //Accede a los dos digitos anteriores respecto al largo de la array
        serieFibonacci[serieFibonacci.length - 1]
    );
  }

  serieFibonacci.shift(); //Quita el primer digito de la serie fib "0"

  return serieFibonacci; //Devuelve la array resultante
}

//Devuelve la cantidad de numeros primos introducida
function primos(cantidad) {
  let numerosPrimos = []; //Almacena los numeros primos
  let numero = 0;
  //Llama a la funcion que devuelve si cada numero es primo
  while (numerosPrimos.length !== cantidad) {
    numero++;
    if (primo(numero)) {
      numerosPrimos.push(numero); //Si el numero es primo lo añade a la array
      console.log(numero);
    }
  }
  return numerosPrimos; //Devuelve la array con todos los numeros que son primos
}

//Devuelve el primer numero primo con el numero de cifras introducido
function primoCifras(cifras) {
  let numero = Math.pow(10, cifras - 1); //Eleva 10 a el numero de cifras - 1
  let found = false;

  //Incrementa numero y itera hasta que encuentra un primo.
  while (!found) {
    //Si el numero es primo cambia found a true, si no se incrementa en 1
    if (primo(numero)) {
      found = true;
    } else {
      numero++;
    }
  }

  return numero; //Devuelve el numero primo
}
